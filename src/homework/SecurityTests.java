import org.bouncycastle.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import java.security.Security;

import javax.crypto.*;
import java.security.*;
import java.math.BigInteger;
import java.util.Scanner;
import org.apache.commons.lang3.RandomStringUtils;

public class SecurityTests {

  // Handles Encryption and Decryption for algorithms with one key
  public static void encryptAndDecrypt(BouncyCastleProvider bc, String algo, String origText) throws Exception {
    KeyGenerator gen = KeyGenerator.getInstance(algo, bc);
    gen.init(128);
    Key key = gen.generateKey();
    Cipher cipher = Cipher.getInstance(algo,bc);
    cipher.init(Cipher.ENCRYPT_MODE,key);
    byte[] encrypted = cipher.doFinal(origText.getBytes());
    //System.out.println("Encrypted with  " + algo + ": " + new String(encrypted));
    cipher.init(Cipher.DECRYPT_MODE,key);
    byte[] decrypted = cipher.doFinal(encrypted);
    System.out.println("Plaintext after " + algo + ": " + new String(decrypted));
    System.out.println();
  }

  // Handles RSA Encryption and Decryption of text and signature verification
  public static void encryptWithRSA(BouncyCastleProvider bc, String origText) throws Exception {
    KeyPairGenerator rsa = KeyPairGenerator.getInstance("RSA", bc);
    KeyPair rsaPair = rsa.generateKeyPair();
    Cipher rsaCipher = Cipher.getInstance("RSA", bc);
    rsaCipher.init(Cipher.ENCRYPT_MODE, rsaPair.getPublic());
    byte[] rsaEncrypted = rsaCipher.doFinal(origText.getBytes());
    //System.out.println("Encrypted with RSA: " + new String(rsaEncrypted));
    rsaCipher.init(Cipher.DECRYPT_MODE, rsaPair.getPrivate());
    byte[] rsaDecrypted = rsaCipher.doFinal(rsaEncrypted);
    System.out.println("Plaintext after RSA: " + new String(rsaDecrypted));
    System.out.println();

    // RSA Signature
    MessageDigest sha = MessageDigest.getInstance("SHA-512", bc);
    byte[] digested = sha.digest(origText.getBytes());
    String strDig = String.format("%0128x", new BigInteger(1, digested));
    //System.out.println("Digested string: " + strDig);
    rsaCipher.init(Cipher.ENCRYPT_MODE, rsaPair.getPublic());
    byte[] digestEncrypted = rsaCipher.doFinal(digested);
    String digEnc = String.format("%0128x", new BigInteger(1, digestEncrypted));
    // System.out.println("Encrypted with RSA: " + digEnc);
    rsaCipher.init(Cipher.DECRYPT_MODE, rsaPair.getPrivate());
    byte[] digestDecrypted = rsaCipher.doFinal(digestEncrypted);
    String digDec = String.format("%0128x", new BigInteger(1, digestDecrypted));
    //System.out.println("Digested String after RSA: " + digDec);
    if (strDig.equals(digDec)) System.out.println("Verification successful.");
    else System.out.println("Verification failed.");
    System.out.println();
  }

  public static void extraCredit(BouncyCastleProvider bc) throws Exception {
    int i;
    String[] randomStrings = new String[100];
    for (i = 0; i < 100; i++){
      randomStrings[i] = RandomStringUtils.random(32); //32 chosen arbitrarily, figured it would be big enough
    }

    Key key;
    Cipher cipher;
    byte[] encrypted;
    KeyGenerator gen = KeyGenerator.getInstance("AES", bc);
    gen.init(128);
    long startTime = System.nanoTime();
    for (i = 0; i <100; i++) {
      key = gen.generateKey();
      cipher = Cipher.getInstance("AES",bc);
      cipher.init(Cipher.ENCRYPT_MODE,key);
      encrypted = cipher.doFinal(randomStrings[i].getBytes());
    }
    long endTime = System.nanoTime();
    long aesTime = endTime - startTime;
    System.out.println("It took " + aesTime+ " nanoseconds to encrypt 100 random Strings using AES");

    startTime = System.nanoTime();
    gen = KeyGenerator.getInstance("Blowfish", bc);
    gen.init(128);
    for (i = 0; i <100; i++) {
      key = gen.generateKey();
      cipher = Cipher.getInstance("Blowfish", bc);
      cipher.init(Cipher.ENCRYPT_MODE,key);
      encrypted = cipher.doFinal(randomStrings[i].getBytes());
    }
    endTime = System.nanoTime();
    long blowfishTime = endTime - startTime;
    System.out.println("It took " + blowfishTime+ " nanoseconds to encrypt 100 random Strings using Blowfish");

    KeyPair rsaPair;
    Cipher rsaCipher;
    byte[] rsaEncrypted;
    startTime = System.nanoTime();
    KeyPairGenerator rsa = KeyPairGenerator.getInstance("RSA", bc);
    for (i = 0; i < 100; i++) {
      rsaPair = rsa.generateKeyPair();
      rsaCipher = Cipher.getInstance("RSA", bc);
      rsaCipher.init(Cipher.ENCRYPT_MODE, rsaPair.getPublic());
      rsaEncrypted = rsaCipher.doFinal(randomStrings[i].getBytes());
    }
    endTime = System.nanoTime();
    long rsaTime = endTime - startTime;
    System.out.println("It took " + rsaTime + " nanoseconds to encrypt 100 random Strings using RSA");
    System.out.printf("\nAES Encryption is %.2f times faster than RSA Encryption\n", ((double)rsaTime/aesTime));
    System.out.printf("Blowfish Encryption is %.2f times faster than RSA Encryption\n", ((double)rsaTime/blowfishTime));
    System.out.printf("Blowfish Encryption is %.2f times faster than AES Encryption\n", ((double)aesTime/blowfishTime));
  }

  public static void main(String args[]) throws Exception {
    BouncyCastleProvider bc = new BouncyCastleProvider();
    Security.addProvider(bc);

    // Get user input
    Scanner sc = new Scanner(System.in);
    System.out.println("Please enter a line to be encrypted:");
    String origText = sc.nextLine();
    sc.close();

    encryptAndDecrypt(bc, "AES", origText);
    encryptAndDecrypt(bc, "Blowfish", origText);
    encryptWithRSA(bc, origText);
    extraCredit(bc);
  }
}
