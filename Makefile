all: build_tools

build_tools:
	javac -cp lib/bcpkix-jdk15on-154.jar:`pwd`:lib/bcprov-ext-jdk15on-154.jar:lib/common-lang3.jar:`pwd` -d build src/homework/*.java

run:
	java -cp lib/bcpkix-jdk15on-154.jar:lib/bcprov-ext-jdk15on-154.jar:lib/common-lang3.jar:build/ SecurityTests
